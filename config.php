<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// HTTP
define('HTTP_SERVER', 'http://89.116.174.125:7000/Decormix/');

// HTTPS
define('HTTPS_SERVER', 'http://89.116.174.125:7000/Decormix/');

// DIR
define('DIR_APPLICATION', 'C:/www/Demo/decormix/catalog/');
define('DIR_SYSTEM', 'C:/www/Demo/decormix/system/');
define('DIR_IMAGE', 'C:/www/Demo/decormix/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'decocart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');