<?php

// Heading
$_['heading_title']                     = 'Bankinis pavedimas';

// Text
$_['text_payment']                      = 'Mokėjimas';
$_['text_success']                      = 'Sėkmingai modifikuotas bankinio pavedimo mokėjimo modulis!';
$_['text_edit']                         = 'Koreguoti bankinį pavedimą';

// Entry
$_['entry_bank']                        = 'Bankinio pavedimo instrukcijos';
$_['entry_total']                       = 'Viso';
$_['entry_order_status']                = 'Užsakymo būsena';
$_['entry_geo_zone']                    = 'Geo zona';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_total']                        = 'Suma, nuo kurios šis apmokėjimo būdas tampa aktyvus.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturi teisių modifikuoti bankinio pavedimo mokėjimo modulio!';
$_['error_bank']                        = 'Bankinio pavedimo instrukcijos reikalingos!';
