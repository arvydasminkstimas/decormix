<?php

// Heading
$_['heading_title']                     = 'Nemokamas atsiskaitymas';

// Text
$_['text_payment']                      = 'Mokėjimas';
$_['text_success']                      = 'Sėkmingai modifikuotas nemokamo atsiskaitymo mokėjimo modulis!';
$_['text_edit']                         = 'Koreguoti nemokamą atsiskaitymą';

// Entry
$_['entry_order_status']                = 'Užsakymo būsena';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturi teisių modifikuoti nemokamo atsiskaitymo mokėjimo modulio!';
