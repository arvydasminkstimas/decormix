<?php

// Heading
$_['heading_title']                     = 'Mokėjimas grynaisias';

// Text
$_['text_payment']                      = 'Mokėjimas';
$_['text_success']                      = 'Sėkmingai modifikuotas mokėjimas grynaisias mokėjimo modulis!!';
$_['text_edit']                         = 'Koreguoti mokėjimą grynaisiais';

// Entry
$_['entry_total']                       = 'Suma';
$_['entry_order_status']                = 'Užsakymo būsena';
$_['entry_geo_zone']                    = 'Geo Zona';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_total']                        = 'Minimali suma, kuri aktyvuos šį mokėjimo būdą.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturi teisių modifikuoti mokėjimo grynaisiais mokėjimo modulio!';
