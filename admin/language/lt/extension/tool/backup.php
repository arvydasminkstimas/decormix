<?php

// Heading
$_['heading_title']                     = 'Kopijuoti / Atstatyti';

// Text
$_['text_backup']                       = 'Parsiųsti atsarginę kopiją';
$_['text_success']                      = 'Sėkmingai importuota duomenų bazė!';
$_['text_list']                         = 'Įkėlimų sąrašas';

// Entry
$_['entry_restore']                     = 'Įkelkite atsarginę kopiją';
$_['entry_backup']                      = 'Atstatyti';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti atsarginių kopijų!';
$_['error_backup']                      = 'Įspėjimas: Jūs privalote pasirinkti bent vieną lentelę!';
$_['error_empty']                       = 'Įspėjimas: įkeltas failas tuščias!';
