<?php

// Heading
$_['heading_title']                     = 'Captcha';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotas captcha!';
$_['text_list']                         = 'Captcha sąrašas';

// Column
$_['column_name']                       = 'Captcha pavadinimas';
$_['column_status']                     = 'Būsena';
$_['column_action']                     = 'Veiksmas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti captcha!';
