<?php

// Heading
$_['heading_title']                     = 'Mokėjimai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti mokėjimai!';
$_['text_list']                         = 'Mokėjimų sąrašas';

// Column
$_['column_name']                       = 'Mokėjimo metodai';
$_['column_status']                     = 'Būsena';
$_['column_sort_order']                 = 'Rikiavimo eiliškumas';
$_['column_action']                     = 'Veiksmas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti mokėjimų!';
