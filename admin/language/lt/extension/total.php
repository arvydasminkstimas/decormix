<?php

// Heading
$_['heading_title']                     = 'Bendri užsakymai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti viso užsakymai!';
$_['text_list']                         = 'Bendrų užsakymų sąrašas';

// Column
$_['column_name']                       = 'Bendri užsakymai';
$_['column_status']                     = 'Būsena';
$_['column_sort_order']                 = 'Rikiavimo eiliškumas';
$_['column_action']                     = 'Veiksmas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti viso užsakymų!';
