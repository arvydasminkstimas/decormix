<?php

// Heading
$_['heading_title']                     = 'Citylink';

// Text
$_['text_shipping']                     = 'Pristatymas';
$_['text_success']                      = 'Success: You have modified Citylink shipping!';
$_['text_edit']                         = 'Edit Citylink Shipping';

// Entry
$_['entry_rate']                        = 'Citylink Rates';
$_['entry_tax_class']                   = 'Mokesčių klasė';
$_['entry_geo_zone']                    = 'Geo zona';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_rate']                         = 'Enter values upto 5,2 decimal places. (12345.67) Example: .1:1,.25:1.27 - Weights less than or equal to 0.1Kg would cost &poun';

// Error
$_['error_permission']                  = 'Warning: You do not have permission to modify Citylink shipping!';
