<?php

// Heading
$_['heading_title']                     = 'Google svetainės žemėlapis';

// Text
$_['text_feed']                         = 'Sklaidos kanalas';
$_['text_success']                      = 'Sėkmingai modifikuotas Google svetainės žemėlapio skalidos kanalas!';
$_['text_edit']                         = 'Koreguoti Google svetainės žemėlapį';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_data_feed']                   = 'Duomenų srauto adresas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti Google svetainės žemėlapio skalidos kanalo!';
