<?php

// Heading
$_['heading_title']                     = 'Google Base';

// Text
$_['text_feed']                         = 'Sklaidos kanalas';
$_['text_success']                      = 'Sėkmingai modifikuotas Google Base sklaidos kanalas!';
$_['text_edit']                         = 'Koreguoti Google Base';
$_['text_import']                       = 'Norėdami parsisiųsti naujausią Google kategorijų sąrašą <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">spauskite čia</a> ir pasirinkti taksonomiją su skaitiniais ID .tx) faile. Įkelkite spaudžiant žalią importo mygtuką.';

// Column
$_['column_google_category']            = 'Google kategorija';
$_['column_category']                   = 'Kategorija';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_google_category']             = 'Google kategorija';
$_['entry_category']                    = 'Kategorija';
$_['entry_data_feed']                   = 'Duomenų srauto adresas';
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti Google Base sklaidos kanalą!';
$_['error_upload']                      = 'Failas negalėjo būti įkeltas!';
$_['error_filetype']                    = 'Netinkamas failo tipas!';
