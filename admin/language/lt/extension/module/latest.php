<?php

// Heading
$_['heading_title']                     = 'Naujausios prekės';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas naujausių prekių modulis!';
$_['text_edit']                         = 'Koreguoti naujausių prekių modulį';

// Entry
$_['entry_name']                        = 'Modulio pavadinimas';
$_['entry_limit']                       = 'Rodomų prekių limitas';
$_['entry_width']                       = 'Plotis';
$_['entry_height']                      = 'Aukštis';
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti naujausių prekių modulio!';
$_['error_name']                        = 'Modulio pavadinimas turi būti nuo 3 iki 64 simbolių!';
$_['error_width']                       = 'Plotis reikalingas!';
$_['error_height']                      = 'Aukštis reikalingas!';
