<?php

// Heading
$_['heading_title']                     = 'Partnerystės programa';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas partnerystės programos modulis!';
$_['text_edit']                         = 'Koreguoti partnerystės programos modulį';

// Entry
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti partnerystės programos modulio!';
