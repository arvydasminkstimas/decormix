<?php

// Heading
$_['heading_title']                     = 'Kategorijos';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas kategorijos modulis!';
$_['text_edit']                         = 'Koreguoti kategorijos modulį';

// Entry
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti kategorijos modulio!';
