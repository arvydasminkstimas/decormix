<?php
// Heading
$_['heading_title']    = 'Modalinis';

// Text
$_['text_module']      = 'Papildiniai';
$_['text_success']     = 'Modalinis papildinys s�kmingai pakoreguotas!';
$_['text_edit']        = 'Modalinio papildinio redagavimas';
$_['text_click']       = 'Paspaudimas';
$_['text_auto']        = 'Auto';
$_['text_both']        = 'Abu';
$_['text_large']       = 'Didelis';
$_['text_small']       = 'Ma�as';
$_['text_top']         = 'Vir�us';
$_['text_body']        = 'Apa�ia';
$_['text_footer']      = 'Apatin� eilut�';

// Entry
$_['entry_name']          = 'Vardas';
$_['entry_title']         = 'Pavadinimas';
$_['entry_type']          = 'Tipas';
$_['entry_status']        = 'B�sena';
$_['entry_module']        = 'Papildinys';
$_['entry_activation']    = 'Aktyvacija';
$_['entry_size']          = 'Dydis';
$_['entry_button']        = 'Mygtukas';
$_['entry_expire']        = 'Galiojimas';
$_['entry_position']      = 'Pozicija';
$_['entry_opacity']       = 'Permatomumas';

// Error
$_['error_permission'] = '�sp�jimas: J�s neturite teis�s redaguoti Modalin� papildin�!';
$_['error_name']          = 'Vard� turi sudaryti nuo 3 iki 64 simboli�!';
