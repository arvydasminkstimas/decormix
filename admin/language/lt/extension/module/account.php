<?php

// Heading
$_['heading_title']                     = 'Paskyra';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas paskyros modulis!';
$_['text_edit']                         = 'Redaguoti paskyros modulį';

// Entry
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti paskyros modulio!';
