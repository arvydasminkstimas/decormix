<?php

// Heading
$_['heading_title']                     = 'Informacija';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas informacijos modulis!';
$_['text_edit']                         = 'Koreguoti informacijos modulį';

// Entry
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti informacijos modulio!';
