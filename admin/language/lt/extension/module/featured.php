<?php

// Heading
$_['heading_title']                     = 'Populiariausios prekės';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas populiariausių prekių modulis!';
$_['text_edit']                         = 'Koreguoti populiariausių prekių modulį';

// Entry
$_['entry_name']                        = 'Modulio pavadinimas';
$_['entry_product']                     = 'Prekės';
$_['entry_limit']                       = 'Rodomų prekių limitas';
$_['entry_width']                       = 'Plotis';
$_['entry_height']                      = 'Aukštis';
$_['entry_status']                      = 'Būsena';

// Help
$_['help_product']                      = 'Automatinis užbaigimas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti populiariausių prekių modulio!';
$_['error_name']                        = 'Modulio pavadinimas turi būti nuo 3 iki 64 simbolių!';
$_['error_width']                       = 'Plotis reikalingas!';
$_['error_height']                      = 'Aukštis reikalingas!';
