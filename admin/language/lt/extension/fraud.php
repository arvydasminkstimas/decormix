<?php

// Heading
$_['heading_title']                     = 'Kova su sukčiavimu';

// Text
$_['text_success']                      = 'Sėkmingai modifikuota kova su sukčiavimu!';
$_['text_list']                         = 'Kovos su sukčiavimu sąrašas';

// Column
$_['column_name']                       = 'Kovos su sukčiavimu pavadinimas';
$_['column_status']                     = 'Būsena';
$_['column_action']                     = 'Veiksmas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturi teisių modifikuoti kovos su sukčiavimu!';
