<?php

// Heading
$_['heading_title']                     = 'Sklaidos kanalas';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti sklaidos kanalai!';
$_['text_list']                         = 'Sklaidos kanalų sąrašas';

// Column
$_['column_name']                       = 'Sklaidos kanalo pavadinimas';
$_['column_status']                     = 'Būsena';
$_['column_action']                     = 'Veiksmas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių redaguoti sklaidos kanalų!';
