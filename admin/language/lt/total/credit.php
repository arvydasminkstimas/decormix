<?php

// Heading
$_['heading_title']                     = 'Parduotuvės kreditai';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Sėkmingai modifikuoti parduotuvės kredito suma!';
$_['text_edit']                         = 'Koreguoti parduotuvės viso kreditą';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti parduotuvės kredito sumos!';
