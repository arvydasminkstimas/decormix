<?php

// Heading
$_['heading_title']                     = 'Galutinė suma';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Sėkmingai modifikuota galutinė suma!';
$_['text_edit']                         = 'Koreguoti galutinę sumą';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti galutinės sumos!';
