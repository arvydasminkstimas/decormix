<?php

// Heading
$_['heading_title']                     = 'Mokesčiai';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Sėkmingai modifikuota mokesčių suma!';
$_['text_edit']                         = 'Koreguoti mokesčius';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti mokesčių sumos!';
