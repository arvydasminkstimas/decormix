<?php

// Heading
$_['heading_title']                     = 'Lojalumo taškai';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Sėkmingai modifikuota lojalumo taškų suma!';
$_['text_edit']                         = 'Koreguoti lojalumo taškus';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti lojalumo taškų sumos!';
