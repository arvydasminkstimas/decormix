<?php

// Heading
$_['heading_title']                     = 'Kuponai';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Sėkmingai modifikuoti kuponų užsakymo suma!';
$_['text_edit']                         = 'Koreguoti kuponą';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti kuponų užsakymų sumos!';
