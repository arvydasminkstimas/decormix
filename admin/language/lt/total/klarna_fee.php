<?php

// Heading
$_['heading_title']                     = 'Klarna Fee';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Success: You have modified Klarna fee total!';
$_['text_edit']                         = 'Edit Klarna Fee Total';
$_['text_sweden']                       = 'Švedija';
$_['text_norway']                       = 'Norvegija';
$_['text_finland']                      = 'Suomija';
$_['text_denmark']                      = 'Danija';
$_['text_germany']                      = 'Vokietija';
$_['text_netherlands']                  = 'Olandija';

// Entry
$_['entry_total']                       = 'Užsakymo suma';
$_['entry_fee']                         = 'Mokestis:';
$_['entry_tax_class']                   = 'Mokesčių klasė';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Warning: You do not have permission to modify Klarna fee total!';
