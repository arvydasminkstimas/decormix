<?php

// Heading
$_['heading_title']                     = 'Pakuotės mokestis';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Sėkmingai modifikuotas pakavimo mokesčio suma!';
$_['text_edit']                         = 'Koreguoti pakavimo mokestį';

// Entry
$_['entry_total']                       = 'Užsakymo suma';
$_['entry_fee']                         = 'Mokestis';
$_['entry_tax_class']                   = 'Mokesčių klasė';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_total']                        = 'Atsiskaitymo suma turi pasiekti šią sumą, kol šis pakuotės mokestis taps aktyvus.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti pakuotės mokesčio sumos!';
