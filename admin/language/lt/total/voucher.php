<?php

// Heading
$_['heading_title']                     = 'Dovanų kuponas';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Sėkmingai modifikuota dovanų kuponų suma!';
$_['text_edit']                         = 'Koreguoti dovanų kuponus';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti dovanų kuponų sumos!';
