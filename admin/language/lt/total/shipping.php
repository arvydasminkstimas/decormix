<?php

// Heading
$_['heading_title']                     = 'Pristatymas';

// Text
$_['text_total']                        = 'Bendri užsakymai';
$_['text_success']                      = 'Sėkmingai modifikuota pristatymo suma!';
$_['text_edit']                         = 'Koreguoti pristatymą';

// Entry
$_['entry_estimator']                   = 'Pristatymo įvertinimas';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti pristatymo sumos!';
