<?php
$_['heading_title']          = "Blog'o Kategorijos";
$_['heading_list']           = 'Kategorijos';
$_['heading_form']           = 'Kategorija';

$_['text_success']           = 'Sėkmingai atnaujinote kategorijas';
$_['text_default']           = 'Numatytoji';

$_['column_name']            = 'Pavadinimas';
$_['column_sort_order']      = 'Rikiavimo eilė';
$_['total_products']		= 'Viso prekių';
$_['column_status']          = 'Būklė';
$_['column_action']          = 'Veiksmas';

$_['entry_name']             = 'Pavadinimas';
$_['entry_page_title']       = 'Meta Pavadinimas';
$_['entry_meta_keyword'] 	 = 'Meta raktažodžiai';
$_['entry_meta_description'] = 'Meta aprašymas';
$_['entry_description']      = 'Aprašymas';
$_['entry_parent']           = 'Priklauso kategorijai';
$_['entry_store']            = 'Parduotuvės';
$_['entry_keyword']          = 'SEO URL';
$_['entry_sort_order']       = 'Rikiavimo eilė';
$_['entry_status']           = 'Būklė';
$_['entry_layout']           = 'Valdyti išdėstymą';

$_['error_warning']          = 'Įspėjimas: Patikrinkite klaidas';
$_['error_permission']       = 'Įspėjimas: Jūs neturite teisės koreguoti kategorijų';
$_['error_name']             = 'Kategorijos pavadinimą turi sudaryti nuo 2 iki 32 simbolių';