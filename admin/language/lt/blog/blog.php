<?php

$_['text_blog']     			= "Blog'as";
$_['text_blog_setting']     	= "Blog'o Nustatymai";
$_['text_blog_category']     	= "Blog'o Kategorijos";
$_['text_blog_post']     		= "Blog'o įrašai";
$_['text_blog_comment']     	= "Blog'o Komentarai";

$_['heading_title']     	= "Blog'o įrašai";
$_['heading_form']     		= "Blog'o forma";

$_['text_yes']      	= 'Taip';
$_['text_no']      	= 'Ne';

$_['text_please_select']      	= '--- Prašome pasirinkti ---';

$_['text_success']      	= "Sėkmingai redagavote blog'a";

$_['column_title']      	= 'Pavadinimas';
$_['column_date_added']      	= 'Talpinimo data';
$_['column_comments']      	= 'Komentarai';
$_['column_status']          = 'Būklė';
$_['column_count_read']      	= 'Peržiūrų';

$_['column_sort_order'] 	= 'Rikiavimo eilė';
$_['column_action']     	= 'Veiksmas';

$_['entry_title']       	= 'Pavadinimas';
$_['entry_page_title']       	= 'Meta pavadinimas';
$_['entry_meta_keyword'] 	 = 'Meta raktažodžiai';
$_['entry_meta_description'] = 'Meta aprašymas';
$_['entry_description'] 	= 'Aprašymas';
$_['entry_short_description'] 	= 'Trumpas aprašymas';
$_['entry_short_description_h'] 	= 'This will be used as the blog summary when the blog post is listed in categories, related posts or latest posts.';
$_['entry_tags'] 			= 'Žymos';
$_['entry_category']          = 'Kategorija';
$_['entry_related']     	= 'Susiję įrašai';
$_['entry_status']          = 'Būklė';
$_['entry_store']       	= 'Parduotuvės';
$_['entry_image']       	= 'Paveikslėlis';
$_['entry_author']       	= 'Autorius';
$_['entry_keyword']     	= 'SEO URL';
$_['entry_sort_order']  	= 'Rikiavimo eilė';
$_['entry_allow_comment']          = 'Leisti komentuoti';
$_['entry_comment_permission']          = 'Komentarų leidimai';
$_['entry_comment_need_approval']          = 'Komentarams reikia patvirtinimų';
$_['entry_with_selected']     	= 'Su pasirinktais';
$_['entry_layout']      = 'Išdėtymo valdymas';

$_['error_permission']  	= "Įspėjimas: Jūs neturite teisių redaguoti blog'o!";
$_['error_title']       	= "Pavadinimą turi sudaryti nuo 3 iki 64 simbolių!";
$_['error_description'] 	= 'Aprašymą turi sudaryti ne mažiau 3 simbolių!';
$_['error_account']     	= 'Įspėjimas: This blog page cannot be deleted as it is currently assigned as the default store account terms!';
$_['error_checkout']    	= 'Įspėjimas: This blog page cannot be deleted as it is currently assigned as the default store checkout terms!';
$_['error_store']       	= 'Įspėjimas: This blog page cannot be deleted as its currently used by %s stores!';
$_['error_required_data']   = 'Neįvesti visi privalomi duomenys. Patikrinkite laukus!';