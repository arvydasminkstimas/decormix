<?php
$_['heading_title']       = "Blog'o komentarai";
$_['column_comment']       = 'Komentaras';
$_['column_post']       = "Blog'o įrašai";
$_['column_name']       = 'Autorius';
$_['column_date']       = 'Patalpinimo data';
$_['column_status']       = 'Būklė';
$_['text_no_result']       = 'Nėra komentarų';
$_['text_enable']       = 'Leisti';
$_['text_disable']       = 'Neleisti';
$_['entry_status']        = 'Būklė';
$_['error_permission']    = 'Įspėjimas: Jūs neturite teisės redaguoti komentarų';
$_['text_success']        = 'Jūs sėkmingai koregavote komentarus';
