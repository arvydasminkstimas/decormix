<?php

// Heading
$_['heading_title']                     = 'Grąžinimo priežastys';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotos grąžinimo priežastys!';
$_['text_list']                         = 'Grąžinimo priežasčių sąrašas';
$_['text_add']                          = 'Pridėti grąžinimo priežastį';
$_['text_edit']                         = 'Koreguoti grąžinimo priežastį';

// Column
$_['column_name']                       = 'Grąžinimo priežasties pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Grąžinimo priežasties pavadinimas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti grąžinimo priežasčių!';
$_['error_name']                        = 'Grąžinimo priežasties pavadinimo ilgis turi būti nuo 3 iki 32 simbolių!';
$_['error_return']                      = 'Įspėjimas: Ši priežastis negali būti pašalinta, nes ji yra priskirta grąžintiems produktams (%s)!';
