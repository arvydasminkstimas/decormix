<?php

// Heading
$_['heading_title']                     = 'Mokesčių tarifai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti mokesčių tarifai!';
$_['text_list']                         = 'Mokesčių tarifų sąrašas';
$_['text_add']                          = 'Pridėti mokesčio tarifą';
$_['text_edit']                         = 'Koreguoti mokesčio tarifą';
$_['text_percent']                      = 'Procentai';
$_['text_amount']                       = 'Fiksuota suma';

// Column
$_['column_name']                       = 'Mokesčio pavadinimas';
$_['column_rate']                       = 'Mokesčio tarifas';
$_['column_type']                       = 'Tipas';
$_['column_geo_zone']                   = 'Geo zona';
$_['column_date_added']                 = 'Data';
$_['column_date_modified']              = 'Modifikavimo data';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Mokesčio pavadinimas';
$_['entry_rate']                        = 'Mokesčio tarifas';
$_['entry_type']                        = 'Tipas';
$_['entry_customer_group']              = 'Klientų grupė';
$_['entry_geo_zone']                    = 'Geo zona';

// Error
$_['error_permission']                  = 'Įspėjimas:Jūs neturite teisių modifikuoti mokesčio tarifų!';
$_['error_tax_rule']                    = 'Įspėjimas: Šis mokestis negali būti pašalintas, nes yra priskirtas mokesčių klasėms (%s)!';
$_['error_name']                        = 'Mokesčio pavadinimas turi būti 3-32 simbolių ilgio!';
$_['error_rate']                        = 'Mokesčio tarifas reikalingas!';
