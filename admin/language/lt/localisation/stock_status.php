<?php

// Heading
$_['heading_title']                     = 'Atsargų būsenos';

// Text
$_['text_success']                      = 'Sėkmingai modifikavotos atsargų būsenos!';
$_['text_list']                         = 'Atsargų būsenų sąrašas';
$_['text_add']                          = 'Pridėti atsargų būseną';
$_['text_edit']                         = 'Koreguoti atsargų būseną';

// Column
$_['column_name']                       = 'Atsargų būsenos pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Atsargų būsenos pavadinimas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti atsargų būsenas!';
$_['error_name']                        = 'Prekės būsenos pavadinimas turi būti nuo 3 iki 32 simbolių!';
$_['error_product']                     = 'Įspėjimas: Ši būsena negali būti pašalinta, nes ji yra priskirta %s produktams!';
