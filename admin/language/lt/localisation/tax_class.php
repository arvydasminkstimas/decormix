<?php

// Heading
$_['heading_title']                     = 'Mokesčių klasės';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotos mokesčių klasės!';
$_['text_list']                         = 'Mokesčių klasių sąrašas';
$_['text_add']                          = 'Pridėti mosečių klasę';
$_['text_edit']                         = 'Koreguoti mokesčių klasę';
$_['text_shipping']                     = 'Pristatymo adresas';
$_['text_payment']                      = 'Mokėjimo adresas';
$_['text_store']                        = 'Parduotuvės adresą';

// Column
$_['column_title']                      = 'Mokesčių klasės pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_title']                       = 'Mokesčių klasės pavadinimas';
$_['entry_description']                 = 'Aprašymas';
$_['entry_rate']                        = 'Mokesčio tarifas';
$_['entry_based']                       = 'Priklauso nuo';
$_['entry_geo_zone']                    = 'Geo zona';
$_['entry_priority']                    = 'Prioritetas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti mokesčių klases!';
$_['error_title']                       = 'Mokesčių klasės pavadinimas turi būti nuo 3 iki 32 simbolių!';
$_['error_description']                 = 'Aprašymas turi būti nuo 3 iki 255 simbolių!';
$_['error_product']                     = 'Įspėjimas: Ši mokesčių klasė negali būti pašalinta, nes ji yra priskirta %s produktams!';
