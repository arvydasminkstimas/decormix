<?php

// Heading
$_['heading_title']                     = 'Kalbos';

// Text
$_['text_success']                      = 'Jūs sėkmingai modifikavote kalbas!';
$_['text_list']                         = 'Kalbų sąrašas';
$_['text_add']                          = 'Pridėti kalbą';
$_['text_edit']                         = 'Koreguoti kalbą';

// Column
$_['column_name']                       = 'Kalbos pavadinimas';
$_['column_code']                       = 'Kodas';
$_['column_sort_order']                 = 'Rikiavimo eiliškumas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Kalbos pavadinimas';
$_['entry_code']                        = 'Kodas';
$_['entry_locale']                      = 'Vietovė/koduotė';
$_['entry_image']                       = 'Paveikslėlis';
$_['entry_directory']                   = 'Katalogas';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_code']                         = 'Pvz.: lt, en. Nekeiskite jei tai yra Jūsų numatytoji kalba.';
$_['help_locale']                       = 'Pvz.: lt,lt-lt,lt_LT,en_US.UTF-8,en_US,en-gb,english';
$_['help_image']                        = 'Pvz.: lt.png, gb.png';
$_['help_directory']                    = 'Kalbos katalogo pavadinimas (didžiosios ir mažosios raidės skiriasi).';
$_['help_status']                       = 'Rodyti/slėpti kalbų pasirinkimo sąraše.';

// Error
$_['error_permission']                  = 'Dėmesio: Jūs neturite teisių keisti kalbų!';
$_['error_name']                        = 'Kalbos pavadinimas turi būti nuo 3 iki 32 simbolių!';
$_['error_code']                        = 'Kalbos kodas turi būti ne trumpesnis kaip 2 simboliai!';
$_['error_locale']                      = 'Vietovė/koduotė būtina!';
$_['error_image']                       = 'Paveikslėlio pavadinimas turi būti nuo 3 iki 64 simbolių!';
$_['error_directory']                   = 'Katalogas būtinas!';
$_['error_default']                     = 'Įspėjimas: Ši kalba negali būti pašalinta, nes ji priskirta kaip numatytoji parduotuvės kalba!';
$_['error_admin']                       = 'Įspėjimas: Ši kalba negali būti pašalinta, nes ji priskirta kaip numatytoji administratoriaus kalba!';
$_['error_store']                       = 'Įspėjimas: Ši kalba negali būti pašalinta, nes ji priskirta %s parduotuvei!';
$_['error_order']                       = 'Įspėjimas: Ši kalba negali būti pašalinta, nes ji priskirta %s užsakymui!';
