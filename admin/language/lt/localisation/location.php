<?php

// Heading
$_['heading_title']                     = 'Parduotuvės vietos';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotos parduotuvės vietos!';
$_['text_list']                         = 'Parduotuvės vietų sąrašas';
$_['text_add']                          = 'Pridėti parduotuvės vietą';
$_['text_edit']                         = 'Koreguoti parduotuvės vietą';
$_['text_default']                      = 'Numatytoji parinktis';
$_['text_time']                         = 'Darbo laikas';
$_['text_geocode']                      = 'Geokodavimas nebuvo sėkmingas dėl šios priežasties:';

// Column
$_['column_name']                       = 'Parduotuvės pavadinimas';
$_['column_address']                    = 'Adresas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Parduotuvės pavadinimas';
$_['entry_address']                     = 'Adresas';
$_['entry_geocode']                     = 'Geo kodas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_fax']                         = 'Faksas';
$_['entry_image']                       = 'Paveikslėlis';
$_['entry_open']                        = 'Darbo laikas';
$_['entry_comment']                     = 'Komentaras';

// Help
$_['help_geocode']                      = 'Prašome įvesti Jūsų parduotuvės vietovės geo kodą rankiniu būdu.';
$_['help_open']                         = 'Įveskite Jūsų parduotuvės darbo laiką.';
$_['help_comment']                      = 'Šis laukas yra specialiems pranešimams, kurie informuos Jūsų klientus. Pvz.: Parduotuvė nepriima čekių.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti parduotuvės vietų!';
$_['error_name']                        = 'Parduotuvės pavadinimas turi būti nors 1 simbolio!';
$_['error_address']                     = 'Adresas turi būti nuo 3 iki 128 simbolių!';
$_['error_telephone']                   = 'Telefonas turi būti nuo 3 iki 32 simbolių!';
