<?php

// Heading
$_['heading_title']                     = 'Užsakymo būsenos';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotos užsakymo būsenos!';
$_['text_list']                         = 'Užsakymo būsenų sąrašas';
$_['text_add']                          = 'Pridėti užsakymo buseną';
$_['text_edit']                         = 'Koreguoti užsakymo buseną';

// Column
$_['column_name']                       = 'Užsakymo būsenos pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Užsakymo būsenos pavadinimas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti užsakymo būsenų!';
$_['error_name']                        = 'Užsakymo būsenos pavadinimas turi būti nuo 3 iki 32 simbolių ilgio!';
$_['error_default']                     = 'Įspėjimas: Ši užsakymo būsena negali būti pašalinta, nes yra priskirta kaip numatytoji užsakymo būsena!';
$_['error_download']                    = 'Įspėjimas: Ši užsakymo būsena negali būti pašalinta, nes yra priskirta kaip numatytoji užsakymo parsisiuntimo būsena!';
$_['error_store']                       = 'Įspėjimas: Ši užsakymo būsena negali būti pašalinta, nes yra priskirta %s parduotuvei!';
$_['error_order']                       = 'Įspėjimas: Ši užsakymo būsena negali būti pašalinta, nes yra priskirta %s užsakymui!';
