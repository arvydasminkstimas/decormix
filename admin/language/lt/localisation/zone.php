<?php

// Heading
$_['heading_title']                     = 'Regionai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti regionai!';
$_['text_list']                         = 'Regionų sąrašas';
$_['text_add']                          = 'Pridėto regioną';
$_['text_edit']                         = 'Koreguoti regioną';

// Column
$_['column_name']                       = 'Regiono pavadinimas';
$_['column_code']                       = 'Regiono kodas';
$_['column_country']                    = 'Šalis';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Regiono pavadinimas';
$_['entry_code']                        = 'Regiono kodas';
$_['entry_country']                     = 'Šalis';
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturi teisių modifikuoti regionų!';
$_['error_name']                        = 'Regiono pavadinimas turi būti nuo 3 iki 128 simbolių ilgio!';
$_['error_default']                     = 'Įspėjimas: Šis regionas negali būti pašalintas, nes yra priskirta numatytajam regionui!';
$_['error_store']                       = 'Įspėjimas: Šis regionas negali būti pašalintas, nes yra priskirtas %s parduotuvei!';
$_['error_address']                     = 'Įspėjimas: Šis regionas negali būti pašalintas, nes yra priskirtas %s adresams!';
$_['error_affiliate']                   = 'Įspėjimas: Šis regionas negali būti pašalintas, nes yra priskirtas %s partneriams!';
$_['error_zone_to_geo_zone']            = 'Įspėjimas: Šis regionas negali būti pašalintas, nes yra priskirtas %s regiono geo zonoms!';
