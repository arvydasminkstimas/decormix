<?php

// Heading
$_['heading_title']                     = 'Vartotojai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti vartotojai!';
$_['text_list']                         = 'Vartotojų sąrašas';
$_['text_add']                          = 'Pridėti vartotoją';
$_['text_edit']                         = 'Koreguoti vartotoją';

// Column
$_['column_username']                   = 'Vartotojo vardas';
$_['column_status']                     = 'Būsena';
$_['column_date_added']                 = 'Data';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_username']                    = 'Vartotojo vardas';
$_['entry_user_group']                  = 'Vartotojų grupė';
$_['entry_password']                    = 'Slaptažodis';
$_['entry_confirm']                     = 'Pakartokite slaptažodį';
$_['entry_firstname']                   = 'Vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_email']                       = 'El. paštas';
$_['entry_image']                       = 'Paveikslėlis';
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti vartotojų!';
$_['error_account']                     = 'Įspėjimas: Jūs negalie pašalinti savo paskyros!';
$_['error_exists']                      = 'Įspėjimas: Nurodytas prisijungimo vardas jau yra naudojamas!';
$_['error_username']                    = 'Vartotojo vardas turi būti nuo 3 iki 20 simbolių ilgio!';
$_['error_password']                    = 'Slaptažodis turi būti nuo 3 iki 20 simbolių ilgio!';
$_['error_confirm']                     = 'Naujas slaptažodis ir pakartotas slaptažodis nesutapo!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
