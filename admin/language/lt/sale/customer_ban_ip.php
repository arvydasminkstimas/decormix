<?php

// Heading
$_['heading_title']                     = 'Užblokuoti klientų IP adresai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotas užblokuoto kliento IP adresas!';
$_['text_list']                         = 'Užblokuotų klientų IP adresų sąrašas';
$_['text_add']                          = 'Pridėti kliento IP';
$_['text_edit']                         = 'Koreguoti kliento IP';

// Column
$_['column_ip']                         = 'IP adresas';
$_['column_customer']                   = 'Klientai';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_ip']                          = 'IP adresas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti užblokuotų klientų IP adresų!';
$_['error_ip']                          = 'IP adreso ilgis turi būti nuo 1 iki 15 simbolių!';
