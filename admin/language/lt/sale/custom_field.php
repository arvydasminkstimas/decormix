<?php

// Heading
$_['heading_title']                     = 'Skirtingi laukai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti skirtingi laukai!';
$_['text_list']                         = 'Skirtingų laukų sąrašas';
$_['text_add']                          = 'Pridėti skirtingą lauką';
$_['text_edit']                         = 'Koreguoti skirtingą lauką';
$_['text_choose']                       = 'Išsirinkti';
$_['text_select']                       = 'Pasirinkti';
$_['text_radio']                        = 'Žymės langelis';
$_['text_checkbox']                     = 'Pasirinkimo laukas';
$_['text_input']                        = 'Įvedimas';
$_['text_text']                         = 'Tekstas';
$_['text_textarea']                     = 'Teksto laukas';
$_['text_file']                         = 'Failas';
$_['text_date']                         = 'Data';
$_['text_datetime']                     = 'Data ir laikas';
$_['text_time']                         = 'Laikas';
$_['text_account']                      = 'Paskyra';
$_['text_address']                      = 'Adresas';

// Column
$_['column_name']                       = 'Skirtingo lauko pavadinimas';
$_['column_location']                   = 'Vieta';
$_['column_type']                       = 'Tipas';
$_['column_sort_order']                 = 'Rikiavimo eiliškumas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Skirtingo lauko pavadinimas';
$_['entry_location']                    = 'Vieta';
$_['entry_type']                        = 'Tipas';
$_['entry_value']                       = 'Reikšmė';
$_['entry_custom_value']                = 'Skirtingo lauko reikšmės pavadinimas';
$_['entry_customer_group']              = 'Klientų grupė';
$_['entry_required']                    = 'Privaloma';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_sort_order']                   = 'Naudokite minusą, kad skaičiuotu atvirksčiai nuo paskutinio įrašo.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti skirtingų laukų!';
$_['error_name']                        = 'Skirtingo lauko pavadinimo ilgis turi būti nuo 1 iki 128 simbolių!';
$_['error_type']                        = 'Įspėjimas: Privalote nurodyti skirtingo lauko tipą!';
$_['error_custom_value']                = 'Skirtingos reikšmės pavadinimo ilgis turi būti nuo 1 iki 128 simbolių!';
