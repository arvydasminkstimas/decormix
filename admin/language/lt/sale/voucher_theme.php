<?php

// Heading
$_['heading_title']                     = 'Kuponų temos';

// Text
$_['text_success']                      = 'Sėkmingai modifikuota kupono tema!';
$_['text_list']                         = 'Kuponų temų sąrašas';
$_['text_add']                          = 'Pridėti kupono temą';
$_['text_edit']                         = 'Koreguoti kupono temą';

// Column
$_['column_name']                       = 'Temos pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Temos pavadinimas';
$_['entry_description']                 = 'Temos aprašymas';
$_['entry_image']                       = 'Paveikslėlis';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti kuponų temų!';
$_['error_name']                        = 'Temos pavadinimo ilgis turi būti nuo 3 iki 32 simbolių!';
$_['error_image']                       = 'Paveikslėlis reikalingas!';
$_['error_voucher']                     = 'Įspėjimas: Šios temos negalite pašalinti, nes ji yra priskirta %s kuponams!';
