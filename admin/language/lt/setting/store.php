<?php

// Heading
$_['heading_title']                     = 'Nustatymai';

// Text
$_['text_settings']                     = 'Nustatymai';
$_['text_success']                      = 'Jūs sėkmingai išsaugojote nuostatas!';
$_['text_list']                         = 'Parduotuvių sąrašas';
$_['text_add']                          = 'Pridėti parduotuvę';
$_['text_edit']                         = 'Koreguoti parduotuvę';
$_['text_items']                        = 'Elementai';
$_['text_tax']                          = 'Mokesčiai';
$_['text_account']                      = 'Paskyra';
$_['text_checkout']                     = 'Atsiskaitymas';
$_['text_stock']                        = 'Atsargos';
$_['text_shipping']                     = 'Pristatymo adresas';
$_['text_payment']                      = 'Mokėjimo adresas';

// Column
$_['column_name']                       = 'Parduotuvės pavadinimas';
$_['column_url']                        = 'Parduotuvės nuoroda';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_url']                         = 'Parduotuvės nuoroda';
$_['entry_ssl']                         = 'SSL nuoroda';
$_['entry_name']                        = 'Parduotuvės pavadinimas';
$_['entry_owner']                       = 'Parduotuvės savininkas';
$_['entry_address']                     = 'Adresas';
$_['entry_geocode']                     = 'Geo kodas';
$_['entry_email']                       = 'El. paštas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_fax']                         = 'Faksas';
$_['entry_image']                       = 'Paveikslėlis';
$_['entry_open']                        = 'Darbo valandos';
$_['entry_comment']                     = 'Komentaras';
$_['entry_location']                    = 'Parduotuvės vieta';
$_['entry_meta_title']                  = 'Meta žymos pavadinimas';
$_['entry_meta_description']            = 'Meta žymos aprašymas';
$_['entry_meta_keyword']                = 'Meta žymos raktažodžiai';
$_['entry_layout']                      = 'Numatytasis išdėstymas';
$_['entry_template']                    = 'Šablonas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Regionas';
$_['entry_language']                    = 'Kalba';
$_['entry_currency']                    = 'Valiuta';
$_['entry_product_limit']               = 'Numatytasis prekių kiekis per puslapį (parduotuvėje)';
$_['entry_product_description_length']  = 'Trumpo aprašymo limitas (parduotuvėje)';
$_['entry_tax']                         = 'Rodyti kainas su mokesčiais';
$_['entry_tax_default']                 = 'Naudoti parduotuvės mokesčių adresą';
$_['entry_tax_customer']                = 'Naudoti kliento mokesčių adresą';
$_['entry_customer_group']              = 'Klientų grupė';
$_['entry_customer_group_display']      = 'Klientų grupės';
$_['entry_customer_price']              = 'Rodyti kainas tik prisijungusiems';
$_['entry_account']                     = 'Privatumo politika';
$_['entry_cart_weight']                 = 'Rodyti svorį krepšelio puslapyje';
$_['entry_checkout_guest']              = 'Leisti svečiui apmokėti užsakymą';
$_['entry_checkout']                    = 'Pirkimo sąlygos';
$_['entry_order_status']                = 'Užsakymo būsena';
$_['entry_stock_display']               = 'Rodyti produktų likutį';
$_['entry_stock_checkout']              = 'Neturimo produkto užsakymas';
$_['entry_logo']                        = 'Parduotuvės logotipas';
$_['entry_icon']                        = 'Piktograma';
$_['entry_image_category']              = 'Kategorijos paveikslėlio dydis';
$_['entry_image_thumb']                 = 'Produkto paveikslėlio minatūros dydis';
$_['entry_image_popup']                 = 'Padidinto (popup) produkto atvaizdo dydis';
$_['entry_image_product']               = 'Produktų sąrašo dydis';
$_['entry_image_additional']            = 'Papildomo produkto paveikslėlio dydis';
$_['entry_image_related']               = 'Susijusio produkto paveikslėlio dydis:';
$_['entry_image_compare']               = 'Palyginimo paveikslėlių dydis';
$_['entry_image_wishlist']              = 'Pageidavimų sąrašo paveikslėlių dydis';
$_['entry_image_cart']                  = 'Krepšelio paveikslėlio dydis';
$_['entry_image_location']              = 'Parduotuvės paveikslėlio dydis';
$_['entry_width']                       = 'Plotis';
$_['entry_height']                      = 'Aukštis';
$_['entry_secure']                      = 'Naudoti SSL';

// Help
$_['help_url']                          = 'Įterpkite pilną parduotuvės adresą. Būtnai pridėkite \'/\' pabaigoje. Pvz. http://www.yourdomain.com/path/<br /><br />Nenaudokite katalogų kuriant naujas parduotuves. Visada privalite nurodyti kita domeną arba subdomeną į Jūsų hostingą (serverio talpinimą).';
$_['help_ssl']                          = 'SSL nuoroda į Jūsų parduotuvę. Būtinai pridėkite \'/\' pabaigoje. Pvz. http://www.yourdomain.com/path/<br /><br />Nenaudokite katalogų kuriant naujas parduotuves. Visada privalite nurodyti kita domeną arba subdomeną į Jūsų hostingą (serverio talpinimą).';
$_['help_geocode']                      = 'Prašome įvesti Jūsų parduotuvės vietovės geo kodą rankiniu būdu.';
$_['help_open']                         = 'Įveskite Jūsų parduotuvės darbo laiką.';
$_['help_comment']                      = 'Šis laukas yra specialiems pranešimams, kurie informuos Jūsų klientus. Pvz.: Parduotuvė nepriima čekių.';
$_['help_location']                     = 'Turimų skirtingų parduotuvės vietų pasirinkimas, kurios būs rodomos susisiekite su mumis formoje.';
$_['help_currency']                     = 'Pakeiskite numatytają valiutą. Išvalykite savo naršyklės laikinuosius failus (cache) ir ištrinkite slapukus (cookies), kad matytumėte pakeitimus. ';
$_['help_product_limit']                = 'Nurodo kiek katalogo elementų yra rodoma per puslapį (produktai, kategorijos ir kt.).';
$_['help_product_description_length']   = 'Produktų sąrašo rodyme, trumpo aprašymo simbolių limitas.';
$_['help_tax_default']                  = 'Naudoti parduotuvės adresą mokesčių apskaičiavimui jei niekas nėra prisijungęs. Jūs galite rinktis parduotuvės adresą kaip pristatymo vietą arba mokėtojo adresą.';
$_['help_tax_customer']                 = 'naudoti klientų numatytajį adresą mokesčiams apskaičiuoti kai jie prisijungę. Jūs galite rinktis numatytajį adresą kaip pristatymo vietą arba mokėtojo adresą.';
$_['help_customer_group']               = 'Numatytoji klientų grupė.';
$_['help_customer_group_display']       = 'Rodyti klientų grupes naujiems klientams, tam, kad jie galėtų pasirinkti tinkamą klientų grupę registracijos metu.';
$_['help_customer_price']               = 'Rodyti kainas, tik kai klientas yra prisijungęs.';
$_['help_account']                      = 'Klientai turi sutikti su privatumo politika prieš sukuriant paskyrą.';
$_['help_checkout_guest']               = 'Leisti klientams atlikti užsakymus neprisijungiant. Tai negalios, jei kliento pirkinių krepšelyje bus internetu parsiunčiami produktai.';
$_['help_checkout']                     = 'Klientai turi sutikti su sąlygomis prie atliekant užsakymą.';
$_['help_order_status']                 = 'Nustatyti numatytają užsakymo būseną, kai užsakymas yra apdorotas.';
$_['help_stock_display']                = 'Rodyti produktų kiekį produktų puslapyje.';
$_['help_stock_checkout']               = 'Leisti klientams atlikti užsakymą, net jeigu užsakomos prekės nėra sandėlyje.';
$_['help_icon']                         = 'Piktograma turi būti PNG tipo ir 16px x 16px dydžio.';
$_['help_secure']                       = 'Norint naudoti SSL, patikrinkite ar Jūsų serverio tiekėjas įdiegęs SSL sertifikatą.';

// Error
$_['error_warning']                     = 'Įspėjimas: Atidžiai patikrinkite formos duomenis dėl klaidų!';
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti parduotuves!';
$_['error_name']                        = 'Parduotuvės vardas turi būti nuo 3 iki 32 simbolių ilgio!';
$_['error_owner']                       = 'Parduotuvės savininkas turi būti nuo 3 iki 64 simbolių!';
$_['error_address']                     = 'Parduotuvės adresas turi būti nuo 10 iki 256 simbolių ilgio!';
$_['error_email']                       = 'El. pašto adresas neteisingas!';
$_['error_telephone']                   = 'Telefonas turi būti nuo 3 iki 32 simbolių!';
$_['error_url']                         = 'Parduotuvės nuoroda reikalinga!';
$_['error_meta_title']                  = 'Pavadinimas turi būti nuo 3 iki 32 simbolių!';
$_['error_limit']                       = 'Limitas reikalingas!';
$_['error_customer_group_display']      = 'Privalote nurodyti numatytąją klientų grupę, jei planuojate pasinaudoti šia funkcija!';
$_['error_image_thumb']                 = 'Produkto paveikslėlio miniatiūros dydžio išmatavimai reikalingi!';
$_['error_image_popup']                 = 'Padidinto (popup) produkto paveikslėlio dydžio išmatavimai reikalingi!';
$_['error_image_product']               = 'Produktų sąrašo dydžio išmatavimai reikalingi!';
$_['error_image_category']              = 'Kategorijų sąrašo dydžio išmatavimai reikalingi!';
$_['error_image_additional']            = 'Papildomo produkto paveikslėlio dydžio išmatavimai reikalingi!';
$_['error_image_related']               = 'Susijusio produkto paveikslėlio dydžio išmatavimai reikalingi!';
$_['error_image_compare']               = 'Palyginamo produkto paveikslėlio dydžio išmatavimai reikalingi!';
$_['error_image_wishlist']              = 'Pageidavimų sąrašo paveikslėlių dydžio išmatavimai reikalingi!';
$_['error_image_cart']                  = 'Prekių krepšelio paveikslėlio dydžio išmatavimai reikalingi!';
$_['error_image_location']              = 'Parduotuvės paveikslėlio dydžio išmatavimai reikalingi!';
$_['error_default']                     = 'Įspėjimas: Jūs negalite pašalinti savo numatytosios parduotuvės!';
$_['error_store']                       = 'Įspėjimas: Ši parduotuvė negali būti pašalinta, nes ji priskirta %s užsakymams!';
