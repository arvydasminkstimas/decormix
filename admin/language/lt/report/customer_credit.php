<?php

// Heading
$_['heading_title']                     = 'Klientų kredito ataskaita';

// Text
$_['text_list']                         = 'Klientų kreditų sąrašas';

// Column
$_['column_customer']                   = 'Kliento vardas';
$_['column_email']                      = 'El. paštas';
$_['column_customer_group']             = 'Klientų grupė';
$_['column_status']                     = 'Būsena';
$_['column_total']                      = 'Viso';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_date_start']                  = 'Pradžios data';
$_['entry_date_end']                    = 'Pabaigos data';
