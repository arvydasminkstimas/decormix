<?php

// Heading
$_['heading_title']                     = 'Peržiūrėtų prekių ataskaita';

// Text
$_['text_list']                         = 'Peržiūrėtų prekių sąrašas';
$_['text_success']                      = 'Sėkmingai atstatyta prežiūrėtų prekių ataskaita!';

// Column
$_['column_name']                       = 'Prekės pavadinimas';
$_['column_model']                      = 'Prekės kodas';
$_['column_viewed']                     = 'Peržiūrėta';
$_['column_percent']                    = 'Procentai';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių atstatyti peržiūrėtų prekių ataskaitą! ';
