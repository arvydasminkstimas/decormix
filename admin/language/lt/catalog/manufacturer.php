<?php

// Heading
$_['heading_title']                     = 'Gamintojai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti gamintojai!';
$_['text_list']                         = 'Gamintojų sąrašas';
$_['text_add']                          = 'Pridėti gamintoją';
$_['text_edit']                         = 'Koreguoti gamintoją';
$_['text_default']                      = 'Numatytoji parinktis';
$_['text_percent']                      = 'Procentai';
$_['text_amount']                       = 'Fiksuota suma';

// Column
$_['column_name']                       = 'Gamintojo pavadinimas';
$_['column_sort_order']                 = 'Rikiavimo eiliškumas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Gamintojo pavadinimas';
$_['entry_store']                       = 'Parduotuvės';
$_['entry_keyword']                     = 'SEO nuoroda';
$_['entry_image']                       = 'Paveikslėlis';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';
$_['entry_type']                        = 'Tipas';

// Help
$_['help_keyword']                      = 'Nenaudokite tarpų, tarpus pakeiskite simboliu -. Taip pat įsitikinkite, kad seo raktažodis unikalus.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti gamintojus!';
$_['error_name']                        = 'Gamintojo pavadinimas turi būti nuo 3 iki 64 simbolių!';
$_['error_keyword']                     = 'SEO raktažodis jau naudojamas!';
$_['error_product']                     = 'Įspėjimas: Šis gamintojas negali būti pašalintas, nes yra priskirtas %s prekėms (-ių)!';
