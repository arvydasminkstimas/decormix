<?php

// Heading
$_['heading_title']                     = 'OpenCart';

// Text
$_['text_order']                        = 'Užsakymai';
$_['text_processing_status']            = 'Apdorojama';
$_['text_complete_status']              = 'Baigtas';
$_['text_customer']                     = 'Klientai';
$_['text_online']                       = 'Prisijungę klientai';
$_['text_approval']                     = 'Laukiama patvirtinimo';
$_['text_product']                      = 'Prekės';
$_['text_stock']                        = 'Išparduota';
$_['text_review']                       = 'Atsiliepimai';
$_['text_return']                       = 'Grąžinimai';
$_['text_affiliate']                    = 'Partnerystė';
$_['text_store']                        = 'Parduotuvės';
$_['text_front']                        = 'Parduotuvės vitrina';
$_['text_help']                         = 'Pagalba';
$_['text_homepage']                     = 'Pagrindinis puslapis';
$_['text_support']                      = 'Palaikymo forumas';
$_['text_documentation']                = 'Dokumentacija';
$_['text_logout']                       = 'Atsijungti';
