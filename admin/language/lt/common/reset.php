<?php

// Heading
$_['heading_title']                     = 'Atstatyti slaptažodį';

// Text
$_['text_reset']                        = 'Atstatyti slaptažodį!';
$_['text_password']                     = 'Įveskite naują pageidaujamą slaptažodį.';
$_['text_success']                      = 'Sėkmingai atnaujintas slaptažodis.';

// Entry
$_['entry_password']                    = 'Slaptažodis';
$_['entry_confirm']                     = 'Pakartokite slaptažodį';

// Error
$_['error_password']                    = 'Slaptažodžio ilgis turi būti nuo 5 iki 20 simbolių!';
$_['error_confirm']                     = 'Naujas slaptažodis ir pakartotas slaptažodis nesutapo!';
