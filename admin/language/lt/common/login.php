<?php

// Heading
$_['heading_title']                     = 'Administracija';

// Text
$_['text_heading']                      = 'Administracija';
$_['text_login']                        = 'Įveskite prisijungimo duomenis.';
$_['text_forgotten']                    = 'Pamiršote slaptažodį?';

// Entry
$_['entry_username']                    = 'Vartotojo vardas';
$_['entry_password']                    = 'Slaptažodis';

// Button
$_['button_login']                      = 'Prisijungti';

// Error
$_['error_login']                       = 'Neatitinka Vartotojo vardas ir/ar Slaptažodis';
$_['error_token']                       = 'Neteisinga sesijos žyma. Prisijunkite iš naujo.';
