<?php

// Text
$_['text_approve_subject']              = '%s - Jūsų partnerystės paskyra buvo aktyvuota!';
$_['text_approve_welcome']              = 'Sveiki ir ačiū, kad užsiregistravote el. parduotuvėje %s!';
$_['text_approve_login']                = 'Jūsų paskyra sukurta ir Jūs galite prisijungti su savo el. pašto adresu ir slaptažodžiu šiuo adresu:';
$_['text_approve_services']             = 'Po prisijungimo galėsite sugeneruoti sekimo kodus, stebėti gaunamus periodinius mokesčius ir keisti savo paskyros informaciją.';
$_['text_approve_thanks']               = 'Ačiū,';
$_['text_transaction_subject']          = '%s - partnerystės komisinis mokestis';
$_['text_transaction_received']         = 'Jūsų gautas komisinis mokestis: %s!';
$_['text_transaction_total']            = 'Jūsų visų komisinių mokesčių suma: %s.';
