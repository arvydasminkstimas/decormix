<?php

// Heading
$_['heading_title']                     = 'Leidimas nesuteiktas!';

// Text
$_['text_permission']                   = 'Jūs neturite leidimo pasiekti šį puslapį, prašome kreiptis į savo sistemos administratorių.';
