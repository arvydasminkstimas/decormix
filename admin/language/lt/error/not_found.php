<?php

// Heading
$_['heading_title']                     = 'Puslapis nerastas!';

// Text
$_['text_not_found']                    = 'Puslapis, kurio ieškote, nerastas! Prašome susisiekti su administracija, jei problema pasikartoja.';
