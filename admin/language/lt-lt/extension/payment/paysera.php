<?php
// Heading
$_['heading_title']          = 'Paysera';

// Text
$_['text_payment']           = 'Payment';
$_['text_success']           = 'Success: You have modified Paysera account details!';
$_['text_on']                = 'Įjungtas';
$_['text_off']               = 'Išjungtas';
$_['text_edit']              = 'Redaguoti Paysera';
$_['text_paysera']		     = '<img 
                                    src="view/image/payment/paysera.png" 
                                    alt="PAYSERA" 
                                    title="PAYSERA" 
                                    style="height:27px;" 
                                />';

// Entry
$_['entry_project']          = 'Projekto ID:';
$_['entry_sign']             = 'Slaptažodis:';
$_['entry_lang']             = 'Kalba:';
$_['help_lang']              = 'LIT, ENG, RUS:';
$_['entry_total']            = 'Suma:';
$_['help_total']             = 'Galutinė užsakymo suma, nuo kurios mokėjimo būdas tampa aktyvus.';
$_['entry_test']             = 'Testinis režimas:';
$_['entry_new_order_status'] = 'Naujo užsakymo statusas:';
$_['entry_canceled_status']  = 'Atšaukto užsakymo statusas:';
$_['entry_order_status']     = 'Užsakymo statusas po mokėjimo:';
$_['entry_payment_lang']     = 'Pirminė mokėjimo būdų šalis:';
$_['entry_geo_zone']         = 'Geo zona:';
$_['entry_status']           = 'Statusas:';
$_['entry_sort_order']       = 'Rikiavimas:';
$_['entry_paymentlist']      = 'Rodyti mokėjimo būdų sąrašą:';

// Help
$_['help_callback']          = '';

// Error
$_['error_permission']       = 'Warning: You do not have permission to modify payment Paysera!';
$_['error_project']          = 'Project ID Required!';
$_['error_sign']             = 'Signature password Required!';
$_['error_lang']             = 'Lang code Required!';
$_['error_refresh']          = 'Update failed!';