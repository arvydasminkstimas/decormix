<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Контакты';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';
$_['text_newsletter_label']   = 'ПОЛУЧИТЕ ЛУЧШИЕ НАШИ ПРЕДЛОЖЕНИЯ!';
$_['text-newsletter_order'] = 'ЗАКАЗ';
$_['text_newsletter_email_enter'] = 'Введите ваш адрес электронной почты';
$_['text_company_info'] = 'Bыходные данные';
$_['text_about_us'] = "О нас";
$_['text_for_clients'] = "Для клиентов";
$_['text_our_locations'] = "Наши местоположения";
$_['text_contacts'] = "Kонтакты";

