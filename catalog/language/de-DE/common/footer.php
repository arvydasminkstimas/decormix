<?php
/**
 * @version		$Id: footer.php 4989 2017-06-22 09:22:42Z mic $
 * @package		Translation Deutsch
 * @author		mic - http://osworx.net
 * @copyright	2017 OSWorX
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Text
$_['text_information']	= 'Informationen';
$_['text_service']		= 'Kundendienst';
$_['text_extra']		= 'Extras';
$_['text_contact']		= 'Kontakt';
$_['text_return']		= 'Retouren';
$_['text_sitemap']		= 'Seitenübersicht';
$_['text_manufacturer']	= 'Hersteller';
$_['text_voucher']		= 'Geschenkgutscheine';
$_['text_affiliate']	= 'Partner';
$_['text_special']		= 'Angebote';
$_['text_account']		= 'Konto';
$_['text_order']		= 'Auftragsverlauf';
$_['text_wishlist']		= 'Wunschliste';
$_['text_newsletter']	= 'Newsletter';

$_['text_newsletter_label']   = 'GET BESTE UNSERE ANGEBOTE!';
$_['text_newsletter_order'] = 'AUFTRAG';
$_['text_newsletter_email_enter'] = 'Geben sie ihre E-Mailadresse ein';
$_['text_company_info'] = 'Anforderungen';
$_['text_about_us'] = "Über uns";
$_['text_for_clients'] = "Kundenzone";
$_['text_our_locations'] = "Unsere Standorte";
$_['text_contacts'] = "Kontakte";