<?php

// Text
$_['text_information']                  = 'Informacija';
$_['text_service']                      = 'Klientų aptarnavimas';
$_['text_extra']                        = 'Priedai';
$_['text_contact']                      = 'Susisiekite su mumis';
$_['text_return']                       = 'Grąžinimo forma';
$_['text_sitemap']                      = 'Svetainės žemėlapis';
$_['text_manufacturer']                 = 'Prekių ženklai';
$_['text_voucher']                      = 'Dovanų kuponai';
$_['text_affiliate']                    = 'Partnerystės programa';
$_['text_special']                      = 'Akcijos';
$_['text_account']                      = 'Mano paskyra';
$_['text_order']                        = 'Užsakymo istorija';
$_['text_wishlist']                     = 'Pageidavimų sąrašas';
$_['text_newsletter']                   = 'Naujienų prenumerata';
$_['text_newsletter_label']   = 'GAUKITE NAUJAUSIUS PASIŪLYMUS PIRMI!';
$_['text_newsletter_order'] = 'UŽSAKYTI';
$_['text_newsletter_email_enter'] = 'įveskite savo el. pašto adresą';
$_['text_company_info'] = 'Rekvizitai';
$_['text_about_us'] = "Apie mus";
$_['text_for_clients'] = "Klientams";
$_['text_our_locations'] = "Mus rasite";
$_['text_contacts'] = "Kontaktai";

