<?php

// Text
$_['text_home']                         = 'Pagrindinis';
$_['text_wishlist']                     = 'Norų sąrašas (%s)';
$_['text_shopping_cart']                = 'Krepšelis';
$_['text_category']                     = 'Kategorijos';
$_['text_account']                      = 'Prisijungti';
$_['text_register']                     = 'Registruotis';
$_['text_login']                        = 'Prisijungti';
$_['text_order']                        = 'Užsakymo istorija';
$_['text_transaction']                  = 'Sandoriai';
$_['text_download']                     = 'Parsisiuntimai';
$_['text_logout']                       = 'Atsijungti';
$_['text_checkout']                     = 'Atsiskaitymas';
$_['text_search']                       = 'Paieška';
$_['text_all']                          = 'Rodyti viską';
$_['text_contacts']           = 'Kontaktai';
$_['text_underLogo']                    = "Interjero dekoro elementai";
$_['text_discounted_products']          = "Akcijos ir pasiūlymai";
