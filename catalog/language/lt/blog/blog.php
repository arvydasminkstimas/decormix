<?php
// Modules
$_['heading_title_latest'] 	= 	"Naujausi Blog'o įrašai";
$_['heading_title_category'] = 	"Blog'o Kategorijos";
$_['text_show_all'] = 			'Rodyti viską';

// Blog
$_['text_blog'] = 				"Blog'as";
$_['text_filter_by'] = 			"Blog'o irašai pažymėti kaip: ";
$_['text_posted_on'] = 			'Publikavo:';
$_['text_posted_by'] = 			'Autorius:';
$_['text_read'] = 				'Perskaityta kartų:';
$_['text_comments'] = 			'Komentarai';
$_['text_related_blog'] = 		'Susiję įrašai';
$_['text_no_comment'] = 		'Pakomentuok pirmas!';
$_['text_write_comment'] = 		'Rašyti komentarą';
$_['text_no_results'] =                 "Nėra blog'o įrašų";
$_['text_error'] = 				'Nerastas puslapis';
$_['text_read_more'] = 			'Skaityti daugiau';
$_['text_tags'] = 				'Žymos:';
$_['text_write_comment'] = 		'Rašyti komentarą';
$_['email_notification'] = 		"Nauji blog'o komentarai parašyti nuo: %s";
$_['text_share_this'] = 		'Pasidalinti:';

// Comment
$_['entry_name'] = 				'Vardas';
$_['entry_email'] = 			'E-mail';
$_['entry_comment'] = 			'Komentaras';
$_['entry_captcha'] = 			'Parašyk atsakymą į pateiktą klausimą žemiau:';
$_['button_send'] = 			'Siųsti';
$_['text_success_approve'] = 	'Ačiū! Komentaras sėkmingai išsaugotas ir bus publikuotas po patvirtinimo';
$_['text_success'] = 			'Ačiū! Komentaras sėkmingai išsaugotas';
$_['error_name'] = 				'Vardą turi sudaryti nuo 2 iki 64 simbolių';
$_['error_email'] = 			'Klaida: Nekorektiškai įvestas e-mail adresas';
$_['error_comment'] = 			'Klaida: Komentarą turi sudaryti nuo 5 iki 3000 simbolių';
$_['error_captcha'] = 			'Klaida: Neteisingas patvirtinimo atsakymas';