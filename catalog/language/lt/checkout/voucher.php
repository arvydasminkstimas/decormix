<?php

// Heading
$_['heading_title']                     = 'Naudoti dovanų kuponą';

// Text
$_['text_success']                      = 'Sėkmingai pritaikėte dovanų kupono nuolaidą!';

// Entry
$_['entry_voucher']                     = 'Įveskite kupono kodą';

// Error
$_['error_voucher']                     = 'Įspėjimas: dovanų kuponas netinkamas arba balansas jau išnaudotas!';
$_['error_empty']                       = 'Įspėjimas: prašome įvesti dovanų kupono kodą!';
