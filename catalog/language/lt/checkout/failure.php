<?php

// Heading
$_['heading_title']                     = 'Apmokėjimas nepavyko!';

// Text
$_['text_basket']                       = 'Krepšelis';
$_['text_checkout']                     = 'Atsiskaitymas';
$_['text_failure']                      = 'Apmokėjimas nepavyko';
$_['text_message']                      = '<p>Iškilo problema apdorojant Jūsų apmokėjimą ir užsakymas nebuvo baigtas</p><p>Galimos priežastys:</p> <ul><li>Nepakankamos lėšos</li><li>Patvirtinimas nepavyko</li></ul>';
