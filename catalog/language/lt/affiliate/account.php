<?php

// Heading
$_['heading_title']                     = 'mano partnerystės paskyra';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_my_account']                   = 'mano partnerystės paskyra';
$_['text_my_tracking']                  = 'Mano stebėjimo informacija';
$_['text_my_transactions']              = 'Mano sandoriai';
$_['text_edit']                         = 'Koreguoti paskyros informaciją';
$_['text_password']                     = 'Keisti slaptažodį';
$_['text_payment']                      = 'Keisti apmokėjimo nustatymus';
$_['text_tracking']                     = 'Išskirtinis partnerio stebėjimo kodas';
$_['text_transaction']                  = 'Sandorių istorija';
