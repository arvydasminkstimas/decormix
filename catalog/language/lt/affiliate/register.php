<?php

// Heading
$_['heading_title']                     = 'Partnerystės programa';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_register']                     = 'Partnerio registracija';
$_['text_account_already']              = 'Jei jau turite paskyrą, prašome <a href="%s">prisijungtiį</a>.';
$_['text_signup']                       = 'Norėdami sukurti parnerio paskyrą, užpildykite visus privalomus formos laukus:';
$_['text_your_details']                 = 'Jūsų asmeninė informacija';
$_['text_your_address']                 = 'Jūsų ddreso duomenys';
$_['text_payment']                      = 'Apmokėjimo informacija';
$_['text_your_password']                = 'Jūsų slaptažodis';
$_['text_cheque']                       = 'Čekis';
$_['text_paypal']                       = 'PayPal';
$_['text_bank']                         = 'Banko pavedimas';
$_['text_agree']                        = 'Susipažinau ir sutinku su <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_firstname']                   = 'Jūsų vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_email']                       = 'El. paštas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_fax']                         = 'Faksas';
$_['entry_company']                     = 'Įmonė';
$_['entry_website']                     = 'Tinklalapis';
$_['entry_address_1']                   = 'Adresas 1';
$_['entry_address_2']                   = 'Adresas 2';
$_['entry_postcode']                    = 'Pašto kodas';
$_['entry_city']                        = 'Miestas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Regionas';
$_['entry_tax']                         = 'Mokesčių ID';
$_['entry_payment']                     = 'Apmokėjimo metodas';
$_['entry_cheque']                      = 'Čekio gavėjas';
$_['entry_paypal']                      = 'PayPal paskyros el. paštas';
$_['entry_bank_name']                   = 'Banko pavadinimas';
$_['entry_bank_branch_number']          = 'ABA/BSB numeris (padalinio numeris)';
$_['entry_bank_swift_code']             = 'SWIFT kodas';
$_['entry_bank_account_name']           = 'Sąskaitos savininkas';
$_['entry_bank_account_number']         = 'Sąskaitos numeris';
$_['entry_password']                    = 'Slaptažodis';
$_['entry_confirm']                     = 'Patvirtinkite slaptažodį';

// Error
$_['error_exists']                      = 'Įspėjimas: el. pašto adresas jau užregistruotas!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_email']                       = 'El. pašto adresas įvestas klaidingai.';
$_['error_telephone']                   = 'Telefono numeris turi būti nuo 3 iki 32 simbolių!';
$_['error_password']                    = 'Slaptažodis turi būti nuo 3 iki 20 simbolių ilgio!';
$_['error_confirm']                     = 'Slaptažodžio patvirtinimas nesutampa su įvestu slaptažodžiu!';
$_['error_address_1']                   = 'Adresas 1 turi būti nuo 3 iki 128 simbolių!';
$_['error_city']                        = 'Miestas turi būti nuo 2 iki 128 simbolių!';
$_['error_country']                     = 'Pasirinkite šalį!';
$_['error_zone']                        = 'Pasirinkite regioną!';
$_['error_postcode']                    = 'Pašto kodas turi būti nuo 2 iki 10 simbolių!';
$_['error_agree']                       = 'Įspėjimas: Jūs turite sutikti su %s!';
