<?php

// Heading
$_['heading_title']                     = 'Apmokėjimo metodas';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_payment']                      = 'Apmokėjimas';
$_['text_your_payment']                 = 'Apmokėjimo informacija';
$_['text_your_password']                = 'Jūsų slaptažodis';
$_['text_cheque']                       = 'Čekis';
$_['text_paypal']                       = 'PayPal';
$_['text_bank']                         = 'Banko pavedimas';
$_['text_success']                      = 'Sėkmingai atnaujinta paskyros informacija.';

// Entry
$_['entry_tax']                         = 'Mokesčių ID';
$_['entry_payment']                     = 'Apmokėjimo metodas';
$_['entry_cheque']                      = 'Čekio gavėjas';
$_['entry_paypal']                      = 'PayPal paskyros el. paštas';
$_['entry_bank_name']                   = 'Banko pavadinimas';
$_['entry_bank_branch_number']          = 'ABA/BSB numeris (padalinio numeris)';
$_['entry_bank_swift_code']             = 'SWIFT kodas';
$_['entry_bank_account_name']           = 'Sąskaitos savininkas';
$_['entry_bank_account_number']         = 'Sąskaitos numeris';
