<?php

// Text
$_['text_subject']                      = '%s - Dėkojame Jums už registraciją';
$_['text_welcome']                      = 'Sveiki ir ačiū, kad užsiregistravote el. parduotuvėje %s!';
$_['text_login']                        = 'Jūsų paskyra sukurta ir Jūs galite prisijungti su savo el. pašto adresu ir slaptažodžiu šiuo adresu:';
$_['text_approval']                     = 'Jūsų paskyra turi būti patvirtinta. Ją patvirtinus, galėsite prisijungti nurodę savo el. pašto adresą ir slaptažodį:';
$_['text_services']                     = 'Prisijungę prie paskyros galėsite peržiūrėti visus savo užsakymus, atsispausdinti sąskaitas, redaguoti kontaktinę informaciją.';
$_['text_thanks']                       = 'Ačiū,';
$_['text_new_customer']                 = 'Naujas klientas';
$_['text_signup']                       = 'Užsiregistravo naujas klientas:';
$_['text_website']                      = 'Tinklalapis:';
$_['text_customer_group']               = 'Klientų grupė:';
$_['text_firstname']                    = 'Vardas:';
$_['text_lastname']                     = 'Pavardė:';
$_['text_email']                        = 'El. paštas:';
$_['text_telephone']                    = 'Telefonas:';
