<?php

// Text
$_['text_new_subject']                  = '%s - Užsakymas %s';
$_['text_new_greeting']                 = 'Ačiū, kad susidomėjote parduotuvės "%s" prekėmis. Mes gavome Jūsų užsakymą ir jis bus pradėtas vykdyti iš karto po apmokėjimo patvirtinimo.';
$_['text_new_received']                 = 'Gavote naują užsakymą.';
$_['text_new_link']                     = 'Norėdami pamatyti savo užsakymą, paspauskite žemiau esančią nuorodą:';
$_['text_new_order_detail']             = 'Užsakymo detalės';
$_['text_new_instruction']              = 'Instrukcijos';
$_['text_new_order_id']                 = 'Užsakymo ID:';
$_['text_new_date_added']               = 'Data:';
$_['text_new_order_status']             = 'Užsakymo būsena:';
$_['text_new_payment_method']           = 'Apmokėjimo metodas:';
$_['text_new_shipping_method']          = 'Pristatymo metodas:';
$_['text_new_email']                    = 'El. pašto adresas:';
$_['text_new_telephone']                = 'Telefonas:';
$_['text_new_ip']                       = 'IP adresas:';
$_['text_new_payment_address']          = 'Mokėjimo adresas';
$_['text_new_shipping_address']         = 'Pristatymo adresas';
$_['text_new_products']                 = 'Produktai';
$_['text_new_product']                  = 'Prekė';
$_['text_new_model']                    = 'Prekės kodas';
$_['text_new_quantity']                 = 'Kiekis';
$_['text_new_price']                    = 'Kaina';
$_['text_new_order_total']              = 'Bendri užsakymai';
$_['text_new_total']                    = 'Iš viso';
$_['text_new_download']                 = 'Patvirtinus Jūsų apmokėjimą, galėsite parsisiųsti užsakytus failus šiuo adresu:';
$_['text_new_comment']                  = 'Užsakymo komentarai:';
$_['text_new_footer']                   = 'Prašome atsakyti į šį laišką, jeigu turite kokių nors klausimų.';
$_['text_update_subject']               = '%s - Užsakymo atnaujinimas %s';
$_['text_update_order']                 = 'Užsakymo ID:';
$_['text_update_date_added']            = 'Užsakymo data:';
$_['text_update_order_status']          = 'Jūsų užsakymo būsena buvo atnaujinta į:';
$_['text_update_comment']               = 'Užsakymo komentarai:';
$_['text_update_link']                  = 'Norėdami pamatyti savo užsakymą, paspauskite žemiau esančią nuorodą:';
$_['text_update_footer']                = 'Prašome atsakyti į šį laišką, jei turite kokių nors klausimų ar neaiškumų.';
