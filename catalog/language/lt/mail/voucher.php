<?php

// Text
$_['text_subject']                      = 'Jūms atsiųstas dovanų kuponas iš %s';
$_['text_greeting']                     = 'Sveikiname, Jūs gavote dovanų kuponą, kurio vertė %s';
$_['text_from']                         = 'Šis dovanų kuponas buvo atsiųstas Jums nuo %s';
$_['text_message']                      = 'Prie kupono palikta žinutę: ';
$_['text_redeem']                       = 'Tam, kad galėtumėte pasinaudoti šiuo dovanų kuponu, turite paspausti žemiau esančią nuorodą, išsirinkti norimas prekes, įsidėti jas į krepšelį ir, prieš tvirtinant užsakymą, įveskite dovanų sertifikato kodą <b>%s</b>.';
$_['text_footer']                       = 'Prašome atsakyti į šį laišką, jeigu turite kokių nors klausimų.';
