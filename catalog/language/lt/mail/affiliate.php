<?php

// Text
$_['text_subject']                      = '%s - Partnerystės programa';
$_['text_welcome']                      = 'Ačiū, kad prisijungėte prie %s Partnerystės programos!';
$_['text_login']                        = 'Jūsų paskyra buvo sukurta ir galite prisijungti naudojant savo el. pašto adresą ir slaptažodį apsilankę mūsų svėtainėje arba šiuo adresu:';
$_['text_approval']                     = 'Jūsų paskyra turi būti patvirtinta prieš jungiantis. Patvirtinus Jūsų paskyrą galėsite prisijungti naudodamiesi registracijos metu nurodytu el. pašto adresu ir slaptažodžiu:';
$_['text_services']                     = 'Po prisijungimo galėsite sugeneruoti sekimo kodus, stebėti gaunamus periodinius mokesčius ir keisti savo paskyros informaciją.';
$_['text_thanks']                       = 'Ačiū,';
$_['text_new_affiliate']                = 'Nauja partnerystė';
$_['text_signup']                       = 'Užsiregistravo naujas partneris:';
$_['text_store']                        = 'Parduotuvė:';
$_['text_firstname']                    = 'Vardas:';
$_['text_lastname']                     = 'Pavardė:';
$_['text_company']                      = 'Įmonė:';
$_['text_email']                        = 'El. paštas:';
$_['text_telephone']                    = 'Telefonas:';
$_['text_website']                      = 'Tinklalapis:';
$_['text_order_id']                     = 'Užsakymo ID:';
$_['text_transaction_subject']          = '%s - partnerystės komisinis mokestis';
$_['text_transaction_received']         = 'Jūsų gautas komisinis mokestis: %s!';
$_['text_transaction_total']            = 'Jūsų visų komisinių mokesčių suma: %s.';
