<?php

// Text
$_['text_title']                        = 'Credit Card / Debit Card (BluePay)';
$_['text_credit_card']                  = 'Card Details';
$_['text_description']                  = 'Items on %s Order No: %s';
$_['text_card_type']                    = 'Card Type: ';
$_['text_card_name']                    = 'Card Name: ';
$_['text_card_digits']                  = 'Last Digits: ';
$_['text_card_expiry']                  = 'Expiry: ';
$_['text_transaction_error']            = 'There was an error processing your transaction - ';

// Entry
$_['entry_card']                        = 'New or Existing Card: ';
$_['entry_card_existing']               = 'Existing';
$_['entry_card_new']                    = 'Naujas';
$_['entry_card_save']                   = 'Remember Card Details';
$_['entry_cc_owner']                    = 'Kortelės savininkas';
$_['entry_cc_number']                   = 'Kortelės numeris';
$_['entry_cc_start_date']               = 'Card Valid From Date';
$_['entry_cc_expire_date']              = 'Kortelės galiojimo pabaigos data';
$_['entry_cc_cvv2']                     = 'Kortelės saugos kodas (CVV2)';
$_['entry_cc_address']                  = 'Street Address';
$_['entry_cc_city']                     = 'Miestas';
$_['entry_cc_state']                    = 'State';
$_['entry_cc_zipcode']                  = 'Zipcode';
$_['entry_cc_phone']                    = 'Phone';
$_['entry_cc_email']                    = 'E-mail';
$_['entry_cc_choice']                   = 'Choose an Existing Card';
