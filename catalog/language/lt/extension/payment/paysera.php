<?php
// Heading
$_['heading_title']      = 'Paysera';
$_['text_title'] 	 = 'Paysera.com';

// Text 
$_['text_payment']       = 'Mokėjimas';
$_['text_success']       = 'Jūs sėkmingai pakoregavote Paysera mokėtojo duomenis!';
$_['text_development']   = '<span style="color: red;">Ruošiama</span>';
$_['text_successful']    = 'Taip - visada sėkmingai';
$_['text_declined']      = 'Taip - visada atmesta';
$_['text_off']           = 'Išjungta';
$_['text_response']     = '';
$_['text_failure']      = '... Jūsų mokėjimas buvo atšauktas!';
$_['text_failure_wait'] = '<b><span style="color: #FF0000">Prašome palaukti...</span></b><br>Jei per 10 nebūsite automatištai nukreiptas, paspauskite čia <a href="%s">here</a>.';										  

      
// Entry 
$_['entry_project']      = 'Projekto ID:';
$_['entry_sign']         = 'Pasirašymo slaptažodis:';
$_['entry_lang']     	 = 'Kalba <small>(LIT, ENG, RUS)<small>:';
$_['entry_test']         = 'Test režimas:';
$_['entry_order_status'] = 'Užsakymo būklė po apmokjimo:';
$_['entry_geo_zone']     = 'Geo zona:';
$_['entry_status']       = 'Būklė:';
$_['entry_sort_order']   = 'Rūšiavimo tvarka:';

// Help
$_['help_callback']      = '';


// Pay Method Select
$_['text_paycountry']    = 'Pasirinkite mokėjimo šalį';
$_['text_chosen']		 = 'Jūs pasirinkote mokėti per Paysera';


// Error
$_['error_permission']   = 'Įspėjimas: Jūs neturite teisės redaguoti Paysera mokėjimų!';
$_['error_project']      = 'Projekto ID Privalomas!';
$_['error_sign']     	 = 'Pasirašymo slaptažodis privalomas!';
$_['error_lang']     	 = 'Kalbos kodas privalomas!';
