<?php

// Heading
$_['heading_title']                     = 'Naudoti lojalumo taškus (Turite %s)';

// Text
$_['text_reward']                       = 'Lojalumo taškai (%s)';
$_['text_order_id']                     = 'Užsakymo ID: #%s';
$_['text_success']                      = 'Sėkmingai pritaikyta lojalumo taškų nuolaida!';

// Entry
$_['entry_reward']                      = 'Naudoti taškų (Maks. %s)';

// Error
$_['error_reward']                      = 'Įspėjimas: įveskite norimą lojalumo taškų kiekį!';
$_['error_points']                      = 'Įspėjimas: Jūs neturite %s lojalumo taškų!';
$_['error_maximum']                     = 'Įspėjimas: maksimalus taškų kiekis kurį galite pritaikyti yra %s!';
