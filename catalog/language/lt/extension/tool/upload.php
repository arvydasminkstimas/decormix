<?php

// Text
$_['text_upload']                       = 'Jūsų failas sėkmingai įkeltas!';

// Error
$_['error_filename']                    = 'Failo vardas turi būti nuo 3 iki 64 simbolių!';
$_['error_filetype']                    = 'Netinkamas failo tipas!';
$_['error_upload']                      = 'Nenurodytas įkeliamas failas!';
