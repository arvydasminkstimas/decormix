<?php
// Heading 
$_['heading_title']		= 'Užsisakyti dabar';

// Text
$_['text_email']		= 'Įveskite e-mail adresą';
$_['text_subscribe']	= 'Sėkmingai užsakytas!';
$_['text_unsubscribe']	= 'Sėkmingai atsisakyta!';
$_['description_newstetter']	= 'Užsisakyte naujienlaiškį ir gaukite geriausius pasiūlymus pirmas.';
$_['text_message']		= '<p>Sėkmingai pašalintas %s iš naujienlaiškių gavėjų.</p>';

// Button 
$_['button_join']		= 'prisijunk!';

// Error
$_['error_email']		= 'Nekorektiškai įvestas e-mail adresas!';
$_['error_unsubscribe']	= '<p>Nepavyko pašalinti iš naujienlaiškių gavėjų. Patikrinkite ar teisingai įvedėte URL adresą.</p>';