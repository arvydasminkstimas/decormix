<?php

// Text
$_['text_success']                      = 'Jūs sėkmingai modifikavote klientus!';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturi teisių pasiektii API!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_email']                       = 'El. pašto adresas įvestas klaidingai.';
$_['error_telephone']                   = 'Telefono numeris turi būti nuo 3 iki 32 simbolių!';
$_['error_custom_field']                = '%s reikalingas!';
