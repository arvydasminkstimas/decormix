<?php

// Text
$_['text_success']                      = 'Sėkmingai modifikuotas krepšelis!';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturi teisių pasiektii API!';
$_['error_stock']                       = 'Produktai pažymėti su *** nėra norimo kiekio arba nėra sandėlyje!';
$_['error_minimum']                     = 'Minimalus užsakymo %s kiekis yra %s!';
$_['error_store']                       = 'Produktai negali būti perkami iš pasirinktos parduotuvės!';
$_['error_required']                    = '%s reikalingas!';
