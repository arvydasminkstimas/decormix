<?php

// Heading
$_['heading_title']                     = 'Mano norų sąrašas';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_instock']                      = 'Sandėlyje';
$_['text_wishlist']                     = 'Norų sąrašas (%s)';
$_['text_login']                        = 'Jūs turite <a href="%s">prisijungti</a> arba <a href="%s">užsiregistruoti</a>, kad galėtumėte įtraukti <a href="%s">%s</a> į savo <a href="%s">norų sąrašą</a>!';
$_['text_success']                      = 'Sėkmingai pridėjote <a href="%s">%s</a> į savo <a href="%s">norų sąrašą</a>!';
$_['text_remove']                       = 'Sėkmingai pašalinote prekę iš savo norų sąrašo!';
$_['text_empty']                        = 'Jūsų norų sąrašas yra tuščias.';

// Column
$_['column_image']                      = 'Paveikslėlis';
$_['column_name']                       = 'Pavadinimas';
$_['column_model']                      = 'Prekės kodas';
$_['column_stock']                      = 'Prieinamumas';
$_['column_price']                      = 'Kaina (vnt.)';
$_['column_action']                     = 'Veiksmas';
