<?php
// header
$_['heading_title']  = 'Atnaujinti slaptažodį';

// Text
$_['text_account']   = 'Vartotojas';
$_['text_password']  = 'Įveskite naują slaptažodį.';
$_['text_success']   = 'Slaptažodis sėkmingai pakeistas.';

// Entry
$_['entry_password'] = 'Slaptažodis';
$_['entry_confirm']  = 'Patvirtinti';

// Error
$_['error_password'] = 'Slaptažodį turi sudaryti nuo 4 iki 20 simbolių!';
$_['error_confirm']  = 'Slaptažodžiai nesutampa!';
$_['error_code']     = 'Slaptažodžio atnaujinimo kodas neteisingas arba jau buvo panaudotas!';