<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_newsletter_label']   = 'GET BEST OUR DEALS!';
$_['text_newsletter_order'] = 'ORDER';
$_['text_newsletter_email_enter'] = 'enter your email address';
$_['text_company_info'] = 'Requisites';
$_['text_about_us'] = "About us";
$_['text_for_clients'] = "Clients zone";
$_['text_our_locations'] = "Our locations";
$_['text_contacts'] = "Contacts";