<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$this->load->model('catalog/information');

		$data['informations'] = array();
		

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		$current = 0;

		foreach ($this->model_catalog_information->getInformations() as $result) {
			$image = "";
			if ($current == 0)
			{
				$image = $server . 'catalog/view/theme/decormix/image/footer-icons/ft-icon-pt.png';
			}
			if ($current == 1)
			{
				$image = $server . 'catalog/view/theme/decormix/image/footer-icons/ft-icon-at.png';
			}
			if ($current == 2)
			{
				$image = $server . 'catalog/view/theme/decormix/image/footer-icons/ft-icon-pr.png';
			}
			if ($current == 3)
			{
				$image = $server . 'catalog/view/theme/decormix/image/footer-icons/ft-icon-gg.png';
			}
			$current++;
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id']),
					'icon'	=> $image
				);
			}
		}
		$data['informations'][] = array(
			'title' => $this->language->get('text_company_info'),
			'href'  => $this->url->link('information/contact'),
			'icon'	=> $server . 'catalog/view/theme/decormix/image/footer-icons/ft-icon-rek.png'
		);

		$data['locations'] = array();

		$this->load->model('localisation/location');

		foreach((array)$this->config->get('config_location') as $location_id) {
			$location_info = $this->model_localisation_location->getLocation($location_id);

			if ($location_info) {
				if ($location_info['image']) {
					$image = $this->model_tool_image->resize($location_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_location_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_location_height'));
				} else {
					$image = false;
				}

				$data['locations'][] = array(
					'location_id' => $location_info['location_id'],
					'name'        => $location_info['name'],
					'address'     => nl2br($location_info['address']),
					'geocode'     => $location_info['geocode'],
					'telephone'   => $location_info['telephone'],
					'fax'         => $location_info['fax'],
					'image'       => $image,
					'open'        => nl2br($location_info['open']),
					'comment'     => $location_info['comment']
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['tracking'] = $this->url->link('information/tracking');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/login', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = ($this->request->server['HTTPS'] ? 'https://' : 'http://') . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		$data['scripts'] = $this->document->getScripts('footer');
		$data['logo_white'] = $server . 'image/catalog/logo/logo_white.png';
		$data['name'] = $this->config->get('config_name');

		$data['facebook'] = $server . 'image/catalog/social/facebook.png';
		$data['twitter'] = $server . 'image/catalog/social/twitter.png';
		$data['linkedin'] = $server . 'image/catalog/social/linkedin.png';
		$data['instagram'] = $server . 'image/catalog/social/instagram.png';
						
		return $this->load->view('common/footer', $data);
	}
}
