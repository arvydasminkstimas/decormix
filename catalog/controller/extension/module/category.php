<?php
class ControllerExtensionModuleCategory extends Controller {
	public function index() {
		$this->load->language('extension/module/category');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			$main_categories = $this->model_catalog_category->getCategories($category['category_id']);
			foreach($main_categories as $main_category)
			{
				$data['categories'][] = $this->GetCategoryData($main_category, $data['category_id']);			
			}
		}

		return $this->load->view('extension/module/category', $data);
	}

	private function GetCategoryData($category, $activeCategoryId)
	{		
		$filter_data = array(
			'filter_category_id'  => $category['category_id'],
			'filter_sub_category' => true
		);
		if ($category['image']) {
			$category_thumb = $this->model_tool_image->resize($category['image'], 200, 200);
		} else {
			$category_thumb = '';
		}
		if ($category['icon']) {
			$category_icon = $this->model_tool_image->resize($category['icon'], 45, 45);
		} else {
			$category_icon = '';
		}
		return array(
			'category_id' => $category['category_id'],
			'name'        => $category['name'],
			'children'    => $this->GetChildCategories($category['category_id']),
			'href'        => $this->url->link('product/category', 'path=' . $category['category_id']),
			'top'		  => false,
			'icon' 		  => $category_icon,
			'image' 	  => $category_thumb,
			'isActive'	  => $this->IsCategoryActive($category['category_id'], $activeCategoryId)
		);
	}

	private function IsCategoryActive($categoryId, $activeCategoryId)
	{
		if ($activeCategoryId == $categoryId)
		{
			return true;
		}
		$children = $this->model_catalog_category->getCategories($categoryId);
		foreach($children as $child) {
			if ($this->IsCategoryActive($child['category_id'], $activeCategoryId))
			{
				return true;
			}
		}
		return false;
	}

	private function GetChildCategories($categoryId)
	{
		$children_data = array();
		$children = $this->model_catalog_category->getCategories($categoryId);
		foreach($children as $child) {
			$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);
			if ($child['image']) {
				$child_thumb = $this->model_tool_image->resize($child['image'], 45, 45);
			} else {
				$child_thumb = '';
			}
			$children_data[] = array(
				'category_id' => $child['category_id'],
				'name' 		  => $child['name'],
				'children'    => $this->GetChildCategories($child['category_id']),
				'href' 		  => $this->url->link('product/category', 'path=' . $categoryId . '_' . $child['category_id']),
				'top' 		  => false,
				'icon' 		  => $child_thumb
			);
		}
		return $children_data;
	}
}